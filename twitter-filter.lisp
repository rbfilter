;; Twitter Filter: A simple filter for custom twitter searches
;; Copyright (C) 2012 Panagiotis Koutsourakis

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(ql:quickload "drakma")
(ql:quickload "cl-json")

(defpackage :twitter-filter
  (:use :cl :drakma :json)
  (:export :parse-query
           :get-tweet-element
           :get-tweet-element-list
           :add-user-to-white-list
           :add-user-to-black-list
           :set-white-list-mode
           :set-black-list-mode
           :apply-lists))

(in-package :twitter-filter)



(defvar *search-url* "http://search.twitter.com/search.json?q="
  "The twitter search API URL")

(defun convert-response-to-string (resp)
  "The server response is an array of integers. This function converts
it to a string."
  (coerce (mapcar #'code-char (coerce resp 'list)) 'string))

;; TODO: Complete for more characters
(defun percent-encode (str)
  "Make the string percent encoded in order to be compatible with
URLs."
  (labels ((percent-encode-char-list (char-list)
             (let ((curr (car char-list))
                   (rest (cdr char-list)))
               (cond 
                 ((null rest) (cons curr nil))
                 ((eq curr #\#)
                      (append '(#\% #\2 #\3) (percent-encode-char-list rest)))
                 (t (cons curr (percent-encode-char-list rest)))))))
    (coerce (percent-encode-char-list (coerce str 'list)) 'string)))

(defun make-query (search-term)
  "Make the actual search query to the twitter API."
  (http-request (concatenate 'string *search-url* (percent-encode search-term))))

(defun get-tweet-element (element tweet)
  "Get an element of a single tweet."
  (cdr (assoc element tweet)))

(defun get-tweet-element-list (element lisp-tweets)
  "Make a list with one element from each tweet from lisp-tweets."
  (mapcar (lambda (tweet) (get-tweet-element element tweet)) lisp-tweets))

(defvar *cache* nil 
  "The tweets we already have gotten from the server.")
(defvar *cache-valid* nil
  "Shows if the cache is valid or needs update.")
(defvar *cache-update-time* 0
  "Last time we got tweets from the server.")
;;; +cache-update-intervar+ depends on the Twitter rate limit. Once a
;;; minute is a good place to start. I should check the streaming API
;;; as well that is not rate limited
(defconstant +cache-update-interval+ 60
  "How long should we wait between two cache updates.")

(defun invalidate-cache (last-update)
  "Check if the time for a cache update has come."
  (let* ((ctime (sb-ext:get-time-of-day))
         (diff  (- ctime last-update)))
    (if (> diff +cache-update-interval+)
        (setf *cache-valid* nil))))

(defun drop-old-tweets (new old)
  "The new tweets coming from the server might contain some of the
tweets we have already shown. Find the set of the new tweets."
  (let* ((old-ids (get-tweet-element-list :id--str old))
         (new-cache 
           (remove-if (lambda (x)
                        (member (get-tweet-element :id--str x) old-ids :test #'equal))
                      new)))
    (if new-cache
        new-cache
        *cache*)))

(defun update-cache (search-term)
  "Update the cache and its update time"
  (let ((server-resp (parse-query search-term)))
    (setf *cache* (drop-old-tweets server-resp *cache*))
    (setf *cache-valid* t)
    (setf *cache-update-time* (sb-ext:get-time-of-day)))
  *cache*)

(defun get-tweets (search-term)
  "Return the tweets either from the cache if it's valid or update it"
  (invalidate-cache *cache-update-time*)
  (if *cache-valid*
      *cache*
      (update-cache search-term)))

(defun parse-query (search-term)
  "Convert the response to Lisp objects using cl-json library."
  (with-input-from-string 
      (s (convert-response-to-string (make-query search-term)))
    (cdr (assoc :results (decode-json s)))))

(defvar *white-list-mode* nil
  "A boolean variable that shows if we are applying the white list.")
(defvar *black-list-mode* nil
  "A boolean variable that shows if we are applying the black list.")

(defvar *white-list* nil
  "A list of users whose tweets will be shown.")
(defvar *black-list* nil
  "A list of users whose tweets will never be shown.")

(defmacro create-filter (name mode-var filt selector)
  `(defun ,name (tweet-list)
     (if ,mode-var
         (,selector (lambda (x)
                      (member (get-tweet-element :from--user x)
                              ,filt :test #'equal))
                    tweet-list)
         tweet-list)))

(create-filter filter-black-list *black-list-mode* *black-list* remove-if)
(create-filter filter-white-list *white-list-mode* *white-list* remove-if-not)


(defun apply-lists (lisp-tweets)
  "Apply the functions white-list and black-list"
  (filter-black-list (filter-white-list lisp-tweets)))

(defmacro make-add-functs (name lst)
  "A simple macro to create functions to add users to the white or the
black list."
  `(defun ,name (user)
     (push user ,lst)))

(defmacro make-set-functs (name mode-var)
  "A simple macro to create functions to change the mode of the white
and the black lists."
  `(defun ,name (mode)
     (setf ,mode-var mode)))

(make-add-functs add-user-to-white-list *white-list*)
(make-add-functs add-user-to-black-list *black-list*)

(make-set-functs set-white-list-mode *white-list-mode*)
(make-set-functs set-black-list-mode *black-list-mode*)

;;; Experimental stuff. Could be useful for the presentation.
;Not strictly necessary... Just a reminder.
(defun convert-tweets-to-json (lisp-tweets)
  (encode-json lisp-tweets))

(defvar *twitter-base* "http://www.twitter.com/#!/")

(defun tweet-to-html (tweet)
  (concatenate 'string *twitter-base*
               (get-tweet-element tweet :from--user)
               "/status/"
               (get-tweet-element tweet :id--str)))
